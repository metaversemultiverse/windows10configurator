﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Windows10Configurator.winget
{
    public partial class ChoosePkgs : Form
    {

        private readonly OpenFileDialog file;

        public ChoosePkgs(OpenFileDialog file)
        {
            InitializeComponent();

            this.file = file;

            string[] packages = File.ReadAllLines(file.FileName);
            for (int i = 0; i < packages.Length; i++)
            {
                appsListBox.Items.Insert(i, packages[i]);
            }
        }

        private void ChoosePkgs_Load(object sender, EventArgs e)
        {
            wingetSelectedFileLbl.Visible = true;
            wingetSelectedFileLbl.Text += this.file.FileName;
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void appsListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            this.BeginInvoke(new Action(() =>
            {
                if (appsListBox.CheckedItems.Count > 0)
                {
                    installBtn.Enabled = true;
                }
                else
                {
                    installBtn.Enabled = false;
                }
            }));
        }

        private void selectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < appsListBox.Items.Count; i++)
            {
                appsListBox.SetItemChecked(i, true);
            }
        }

        private void selectNone_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < appsListBox.Items.Count; i++)
            {
                appsListBox.SetItemChecked(i, false);
            }
        }

        private void invertSelection_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < appsListBox.Items.Count; i++)
            {
                appsListBox.SetItemChecked(i, !appsListBox.GetItemChecked(i));
            }
        }

        private void installBtn_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show($"Are you sure you want to install {appsListBox.CheckedItems.Count} applications?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Install install = new Install(appsListBox.CheckedItems.OfType<string>().ToArray());
                install.ShowDialog();
            }
        }
    }
}
