﻿using System;
using System.Windows.Forms;

namespace Windows10Configurator.winget
{
    class Logic
    {

        public static bool Installed = false;
        public static OpenFileDialog file;

        public static OpenFileDialog ChooseFile()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog1.Filter = "Text files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                return openFileDialog1;
            }

            return null;
        }
    }
}
