﻿namespace Windows10Configurator.winget
{
    partial class Install
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Install));
            this.statusHeaderLbl = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.processedLbl = new System.Windows.Forms.Label();
            this.detailedListView = new System.Windows.Forms.ListView();
            this.Timestamp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.App = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Status = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // statusHeaderLbl
            // 
            this.statusHeaderLbl.AutoSize = true;
            this.statusHeaderLbl.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusHeaderLbl.Location = new System.Drawing.Point(26, 23);
            this.statusHeaderLbl.Name = "statusHeaderLbl";
            this.statusHeaderLbl.Size = new System.Drawing.Size(155, 45);
            this.statusHeaderLbl.TabIndex = 2;
            this.statusHeaderLbl.Text = "Initializing";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(34, 76);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(742, 45);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 3;
            // 
            // processedLbl
            // 
            this.processedLbl.AutoSize = true;
            this.processedLbl.Location = new System.Drawing.Point(30, 128);
            this.processedLbl.Name = "processedLbl";
            this.processedLbl.Size = new System.Drawing.Size(92, 20);
            this.processedLbl.TabIndex = 4;
            this.processedLbl.Text = "Processed: ";
            // 
            // detailedListView
            // 
            this.detailedListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.detailedListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Timestamp,
            this.App,
            this.Status});
            this.detailedListView.HideSelection = false;
            this.detailedListView.Location = new System.Drawing.Point(34, 163);
            this.detailedListView.Name = "detailedListView";
            this.detailedListView.Size = new System.Drawing.Size(742, 289);
            this.detailedListView.TabIndex = 5;
            this.detailedListView.UseCompatibleStateImageBehavior = false;
            this.detailedListView.View = System.Windows.Forms.View.Details;
            // 
            // Timestamp
            // 
            this.Timestamp.Text = "Timestamp";
            // 
            // App
            // 
            this.App.Text = "App";
            // 
            // Status
            // 
            this.Status.Text = "Status";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Install
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 464);
            this.Controls.Add(this.detailedListView);
            this.Controls.Add(this.processedLbl);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.statusHeaderLbl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(830, 520);
            this.Name = "Install";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Downloading & installing...";
            this.Load += new System.EventHandler(this.Install_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label statusHeaderLbl;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label processedLbl;
        private System.Windows.Forms.ListView detailedListView;
        private System.Windows.Forms.ColumnHeader Timestamp;
        private System.Windows.Forms.ColumnHeader App;
        private System.Windows.Forms.ColumnHeader Status;
        private System.Windows.Forms.Timer timer1;
    }
}