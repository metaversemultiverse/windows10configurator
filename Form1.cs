﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Windows.Forms;
using Windows10Configurator.winget;

namespace Windows10Configurator
{
    public partial class Form1 : Form
    {

        public const bool FORCE_ELEVATED_PRIVILEDGES = true;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (!common.Elevated.IsElevated)
            {
                elevatedPanel.Visible = true;

                if (FORCE_ELEVATED_PRIVILEDGES)
                {
                    restartWithElevatedPrivBtn_Click(null, null);
                }
            }

            tabControl1.Dock = DockStyle.Fill;
            aboutVersionLbl.Text = config.App.Version.ToString("0.00").Replace(",", ".");

            PowerShell ps = PowerShell.Create();
            var res = ps.AddCommand("Get-AppxPackage").AddArgument("Microsoft.Winget.Source").Invoke();
            foreach(var i in res)
            {
                if (i.ToString().Contains("Microsoft.Winget.Source"))
                {
                    winget.Logic.Installed = true;
                    break;
                }
            }

            if (winget.Logic.Installed)
            {
                wingetDownloadBtn.Enabled = false;
                wingetInstallStatusLbl.Text = "Already installed!";
                wingetInstallStatusLbl.ForeColor = Color.Green;
                wingetChooseFileBtn.Enabled = true;
            }
            else
            {
                wingetInstallStatusLbl.Text = "About 6 MB";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            wingetDlProgressBar.Visible = true;
            wingetDownloadBtn.Enabled = false;

            common.Download.Go(config.App.DefaultDownloadPath, config.App.WinGetDownloadURL.Split('/').Last(), config.App.WinGetDownloadURL, (s, pp) =>
            {
                wingetDlProgressBar.Value = pp.ProgressPercentage;
            }, (o, i) =>
            {
                wingetDlProgressBar.Visible = false;
                wingetInstallStatusLbl.Text = "Download finished. Installing...";
                using (Process process = new Process())
                {
                    process.StartInfo.FileName = Path.Combine(config.App.DefaultDownloadPath, config.App.WinGetDownloadURL.Split('/').Last());
                    process.EnableRaisingEvents = true;
                   /* process.Exited += (s, ee) =>
                    {*/
                        wingetInstallStatusLbl.Text = "Installed!";
                        wingetInstallStatusLbl.ForeColor = Color.Green;
                        wingetChooseFileBtn.Enabled = true;
                        winget.Logic.Installed = true;
                    /*};*/
                    process.Start();
                }
            });
        }

        private void wingetChooseFileBtn_Click(object sender, EventArgs e)
        {
            winget.Logic.file = winget.Logic.ChooseFile();
            if (winget.Logic.file != null)
            {
                wingetSelectedFileLbl.Visible = true;
                wingetSelectedFileLbl.Text = $"Selected file: {winget.Logic.file.FileName}";
                wingetInstallBtn.Enabled = true;
                wingetInstallBtn_Click(sender, e);
            }
        }

        private void wingetAlreadyInstalledChb_CheckedChanged(object sender, EventArgs e)
        {
            if (winget.Logic.Installed)
            {
                wingetChooseFileBtn.Enabled = true;
            }
        }

        private void restartWithElevatedPrivBtn_Click(object sender, EventArgs e)
        {
            Process proc = new Process();
            proc.StartInfo.FileName = System.Reflection.Assembly.GetEntryAssembly().Location;
            proc.StartInfo.UseShellExecute = true;
            proc.StartInfo.Verb = "runas";
            proc.Start();
            this.Close();
        }

        private void visitDeveloperWebsiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("https://francescosorge.com");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void wingetInstallBtn_Click(object sender, EventArgs e)
        {
            ChoosePkgs choosePkgs = new ChoosePkgs(winget.Logic.file);
            choosePkgs.ShowDialog();
        }

        private void disableBloatwareDlBtn_Click(object sender, EventArgs e)
        {
            disableBloatwareDlProgressBar.Visible = true;
            disableBloatwareDlBtn.Enabled = false;

            common.Download.Go(config.App.DefaultDownloadPath, disableBloatware.Logic.FileName, config.App.Windows10DebloaterDownloadURL, (s, pp) =>
            {
                disableBloatwareDlProgressBar.Value = pp.ProgressPercentage;
            }, (o, i) =>
            {
                disableBloatwareDlProgressBar.Visible = false;
                disableBloatwareInstallStatusLbl.Visible = true;
                disableBloatwareInstallStatusLbl.Text = "Download finished";
                disableBloatwareInstallStatusLbl.ForeColor = Color.Green;
                disableBloatwareExBtn.Enabled = true;
            });
        }

        private void disableBloatwareExBtn_Click(object sender, EventArgs e)
        {
            using (PowerShell PowerShellInstance = PowerShell.Create())
            {
                string script = "Set-ExecutionPolicy -Scope Process -ExecutionPolicy Unrestricted; Get-ExecutionPolicy"; // the second command to know the ExecutionPolicy level
                PowerShellInstance.AddScript(script);
                PowerShellInstance.Invoke();
            }

            disableBloatwareExLbl.Visible = true;

            RunspaceConfiguration runspaceConfiguration = RunspaceConfiguration.Create();
            Runspace runspace = RunspaceFactory.CreateRunspace(runspaceConfiguration);
            runspace.Open();
            Pipeline pipeline = runspace.CreatePipeline();
            Command myCommand = new Command(Path.Combine(config.App.DefaultDownloadPath, disableBloatware.Logic.FileName));
            pipeline.Commands.Add(myCommand);

            try
            {
                pipeline.Invoke();
                disableBloatwareExLbl.Text = "Script completed successfully";
                disableBloatwareExLbl.ForeColor = Color.Green;
            }
            catch (Exception)
            {
                disableBloatwareExLbl.Text = "An error occurred.";
                disableBloatwareExLbl.ForeColor = Color.Red;
            }
        }

        private void disableBloatwareCreditLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://github.com/Sycnex/Windows10Debloater");
        }

        private void miscDisableStartupItemsBtn_Click(object sender, EventArgs e)
        {
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = @"/C %windir%\system32\Taskmgr.exe /7 /startup";
            process.StartInfo = startInfo;
            process.Start();
        }

        private void disableBloatwareListLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://github.com/Sycnex/Windows10Debloater#this-script-will-remove-the-bloatware-from-windows-10-when-using-remove-appxpackageremove-appxprovisionedpackage-and-then-delete-specific-registry-keys-that-are-were-not-removed-beforehand-for-best-results-this-script-should-be-ran-before-a-user-profile-is-configured-otherwise-you-will-likely-see-that-apps-that-should-have-been-removed-will-remain-and-if-they-are-removed-you-will-find-broken-tiles-on-the-start-menu");
        }

        private void aboutVisitAuthorWebsiteBtn_Click(object sender, EventArgs e)
        {
            Process.Start("https://francescosorge.com");
        }

        private void aboutContributeProjectBtn_Click(object sender, EventArgs e)
        {
            Process.Start("https://gitlab.com/fsorge/windows10configurator");
        }
    }
}
