﻿using System.Security.Principal;

namespace Windows10Configurator.common
{
    class Elevated
    {
        public static bool IsElevated
        {
            get
            {
                var id = WindowsIdentity.GetCurrent();
                return id.Owner != id.User;
            }
        }
    }
}
